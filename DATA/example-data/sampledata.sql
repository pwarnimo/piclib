-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: DB_PICLIB
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `tblCamera`
--

LOCK TABLES `tblCamera` WRITE;
/*!40000 ALTER TABLE `tblCamera` DISABLE KEYS */;
INSERT INTO `tblCamera` VALUES (1,'FR-I','May have a light leak',1),(2,'EOS650','None',2),(3,'Super TL1000','None',3),(4,'Nova','None',3);
/*!40000 ALTER TABLE `tblCamera` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblCameraMake`
--

LOCK TABLES `tblCameraMake` WRITE;
/*!40000 ALTER TABLE `tblCameraMake` DISABLE KEYS */;
INSERT INTO `tblCameraMake` VALUES (1,'Yashica'),(2,'Canon'),(3,'Praktica');
/*!40000 ALTER TABLE `tblCameraMake` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblDevProcess`
--

LOCK TABLES `tblDevProcess` WRITE;
/*!40000 ALTER TABLE `tblDevProcess` DISABLE KEYS */;
INSERT INTO `tblDevProcess` VALUES (1,'00:07:00','00:04:00','00:10:30',1),(2,'00:07:30','00:04:00','00:10:30',2),(3,'00:12:00','00:05:00','00:10:30',3),(4,'00:08:00','00:06:00','00:06:00',4);
/*!40000 ALTER TABLE `tblDevProcess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblDeveloper`
--

LOCK TABLES `tblDeveloper` WRITE;
/*!40000 ALTER TABLE `tblDeveloper` DISABLE KEYS */;
INSERT INTO `tblDeveloper` VALUES (1,'1+9',20,2),(2,'1+25',20,4),(3,'1+2',20,1),(4,'C-41',30,5);
/*!40000 ALTER TABLE `tblDeveloper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblDeveloperMake`
--

LOCK TABLES `tblDeveloperMake` WRITE;
/*!40000 ALTER TABLE `tblDeveloperMake` DISABLE KEYS */;
INSERT INTO `tblDeveloperMake` VALUES (1,'ID-11'),(2,'Ilfosol 3'),(3,'LC29'),(4,'Rodinal'),(5,'Tetenal Colortec');
/*!40000 ALTER TABLE `tblDeveloperMake` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblFilm`
--

LOCK TABLES `tblFilm` WRITE;
/*!40000 ALTER TABLE `tblFilm` DISABLE KEYS */;
INSERT INTO `tblFilm` VALUES (1,0,0,'2018-02-01 09:31:12',1,6,7),(2,0,0,'2018-02-01 09:31:12',2,6,7),(3,0,1,'2018-02-01 09:31:12',3,1,5),(4,1,0,'2018-02-01 09:31:12',4,8,5);
/*!40000 ALTER TABLE `tblFilm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblFilmISO`
--

LOCK TABLES `tblFilmISO` WRITE;
/*!40000 ALTER TABLE `tblFilmISO` DISABLE KEYS */;
INSERT INTO `tblFilmISO` VALUES (1,25),(2,50),(3,100),(4,125),(5,200),(6,320),(7,400),(8,800),(9,1600),(10,3200);
/*!40000 ALTER TABLE `tblFilmISO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblFilmMake`
--

LOCK TABLES `tblFilmMake` WRITE;
/*!40000 ALTER TABLE `tblFilmMake` DISABLE KEYS */;
INSERT INTO `tblFilmMake` VALUES (1,'Ilford Delta'),(2,'Ilford HP5+'),(3,'Ilford FP4+'),(4,'Ilford XP2 Super'),(5,'Kodak T-MAX'),(6,'Kodak TRI-X'),(7,'AgfaPhoto APX'),(8,'AgfaPhoto Vista');
/*!40000 ALTER TABLE `tblFilmMake` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblPicture`
--

LOCK TABLES `tblPicture` WRITE;
/*!40000 ALTER TABLE `tblPicture` DISABLE KEYS */;
INSERT INTO `tblPicture` VALUES (1,'Pic 1','2018-02-01 09:40:01','Dummy Pic 1',200,8,1,1,1),(2,'Pic 2','2018-02-01 09:40:01','Dummy Pic 2',60,2,1,1,1),(3,'Pic 1-1','2018-02-01 09:40:01','Dummy Pic 1-1',500,11,2,2,2),(4,'Pic 2-1','2018-02-01 09:40:01','Dummy Pic 2-1',30,4,2,3,3),(5,'Pic 3-1','2018-02-01 09:41:30','Dummy Pic 3-1',30,4,3,4,4);
/*!40000 ALTER TABLE `tblPicture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblPictureSet`
--

LOCK TABLES `tblPictureSet` WRITE;
/*!40000 ALTER TABLE `tblPictureSet` DISABLE KEYS */;
INSERT INTO `tblPictureSet` VALUES (1,'Test Set Yashica','/home/pwarnimo/Pictures/FR-1','2018-02-01 09:34:10'),(2,'Test Set Canon','/home/pwarnimo/Pictures/CAN-1','2018-02-01 09:34:10'),(3,'Test Set TL1000','/home/pwarnimo/Pictures/TL-1','2018-02-01 09:35:44'),(4,'Test Set TL1000 2','/home/pwarnimo/Pictures/TL-2','2018-02-01 09:35:44'),(5,'Test Set Nova','/home/pwarnimo/Pictures/NOV-1','2018-02-01 09:35:44');
/*!40000 ALTER TABLE `tblPictureSet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tblRevision`
--

LOCK TABLES `tblRevision` WRITE;
/*!40000 ALTER TABLE `tblRevision` DISABLE KEYS */;
INSERT INTO `tblRevision` VALUES (1,'r1','No changes','2018-02-01 09:44:35',1),(2,'r1','No changes','2018-02-01 09:44:35',2),(3,'r2','Lightroom edit','2018-02-01 09:44:35',2),(4,'r1','No changes','2018-02-01 09:44:35',3),(5,'r1','No changes','2018-02-01 09:44:35',4),(6,'r1','No changes','2018-02-01 09:44:35',5),(7,'r2','No changes','2018-02-01 09:44:35',5),(8,'r3','No changes','2018-02-01 09:44:35',5);
/*!40000 ALTER TABLE `tblRevision` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-01 10:48:32
