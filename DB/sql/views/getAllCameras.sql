/*---------------------------------------------------------------------------------------------
| View        : getAllCameras
| Author(s)   : Pol Warnimont <pwarnimo@gmail.com>
| Create date : 2018-02-02
| Version     : 0.1
| 
| Description : Predefined view for all cameras.
|
| Changelog
| ---------
|  2018-02-02 : Created procedure.
|
| License information
| -------------------
|  Copyright (C) 2016  Pol Warnimont
|
|  This program is free software; you can redistribute it and/or modify
|  it under the terms of the GNU General Public License as published by
|  the Free Software Foundation; either version 2 of the License, or
|  (at your option) any later version.
|
|  This program is distributed in the hope that it will be useful,
|  but WITHOUT ANY WARRANTY; without even the implied warranty of
|  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
|  GNU General Public License for more details.
|
|  You should have received a copy of the GNU General Public License along
|  with this program; if not, write to the Free Software Foundation, Inc.,
|  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
|
+--------------------------------------------------------------------------------------------*/

CREATE VIEW `getAllCameras` AS
SELECT 
	cam.idCamera, 
	make.dtMakeCaption, 
	cam.dtModel, 
	cam.dtNotes
FROM
	tblCamera AS cam, 
	tblCameraMake AS make
WHERE 
	make.idCameraMake = cam.fiCameraMake
