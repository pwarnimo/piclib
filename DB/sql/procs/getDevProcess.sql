/*---------------------------------------------------------------------------------------------
| Routine     : getDevProcess
| Author(s)   : Pol Warnimont <pwarnimo@gmail.com>
| Create date : 2018-02-02
| Version     : 0.1
| 
| Description : Display the of a developer film combination.
|
| Parameters
| ----------
|  IN  : pFilmID : ID number of the film.
|  IN  : pProcID : ID number of the development process.
|  OUT : pErr    : Error code in a case of a failure.
|                   0 = Query OK
|                  -3 = General SQL error
|                  -4 = General SQL warning
|                  -5 = No data
|
| Changelog
| ---------
|  2018-02-02 : Created procedure.
|
| License information
| -------------------
|  Copyright (C) 2015  Pol Warnimont
|
|  This program is free software: you can redistribute it and/or modify
|  it under the terms of the GNU Affero General Public License as
|  published by the Free Software Foundation, either version 3 of the
|  License, or (at your option) any later version.
|
|  This program is distributed in the hope that it will be useful,
|  but WITHOUT ANY WARRANTY; without even the implied warranty of
|  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
|  GNU Affero General Public License for more details.
|
|  You should have received a copy of the GNU Affero General Public License
|  along with this program.  If not, see <http://www.gnu.org/licenses/>.
|
+--------------------------------------------------------------------------------------------*/

DELIMITER $$

DROP PROCEDURE IF EXISTS getDevProcess $$

CREATE PROCEDURE getDevProcess(
	IN pPicID INT,
	IN pProcID INT,
	OUT pErr INT
)
BEGIN
	DECLARE l_errcode INT DEFAULT 0;

	DECLARE no_data CONDITION FOR 1329;

	DECLARE CONTINUE HANDLER FOR no_data SET l_errcode = -5;
	DECLARE CONTINUE HANDLER FOR sqlexception SET l_errcode = -3;
	DECLARE CONTINUE HANDLER FOR sqlwarning SET l_errcode = -4;

	SET @qry = "
		SELECT 
			fmake.dtMakeCaption, 
			iso.dtISOSpeed, 
			dmake.dtMakeCaption, 
			dev.dtDilution, 
			dev.dtTemperature, 
			proc.dtDevTime, 
			proc.dtFixTime, 
			proc.dtRinseTime
		FROM 
			tblFilm AS film, 
			tblFilmMake AS fmake, 
			tblFilmISO AS iso, 
			tblDeveloperMake AS dmake, 
			tblDeveloper AS dev, 
			tblDevProcess AS proc
		WHERE 
			proc.idDevProcess = ? AND 
			film.idFilm = ? AND 
			fmake.idFilmMake = film.fiFilmMake AND 
			iso.idFilmISO = film.fiFilmISO AND 
			dmake.idDeveloperMake = dev.fiDeveloperMake AND 
			proc.fiDeveloper = dev.idDeveloper
	";

	SET @p1 = pPicID;
	SET @p2 = pProcID;

	PREPARE STMT FROM @qry;
	EXECUTE STMT USING @p1, @p2;

	DEALLOCATE PREPARE STMT;

	SET pErr = l_errcode;
END $$

DELIMITER ;
