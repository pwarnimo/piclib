/*---------------------------------------------------------------------------------------------
| Routine     : ai_insertRevision
| Author(s)   : Pol Warnimont <pwarnimo@gmail.com>
| Create date : 2018-02-02
| Version     : 0.1
| 
| Description : This trigger creates a new revision entry if no revision 
|               exists for a new picture.
|
| Changelog
| ---------
|  2018-02-02 : Created trigger.
|
| License information
| -------------------
|  Copyright (C) 2015  Pol Warnimont
|
|  This program is free software: you can redistribute it and/or modify
|  it under the terms of the GNU Affero General Public License as
|  published by the Free Software Foundation, either version 3 of the
|  License, or (at your option) any later version.
|
|  This program is distributed in the hope that it will be useful,
|  but WITHOUT ANY WARRANTY; without even the implied warranty of
|  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
|  GNU Affero General Public License for more details.
|
|  You should have received a copy of the GNU Affero General Public License
|  along with this program.  If not, see <http://www.gnu.org/licenses/>.
|
+--------------------------------------------------------------------------------------------*/

DROP TRIGGER IF EXISTS ai_insertRevision;

DELIMITER $$

CREATE TRIGGER `DB_PICLIB`.`ai_insertRevision` AFTER INSERT ON `tblPicture` FOR EACH ROW
BEGIN
	IF NOT EXISTS (SELECT 1 FROM tblRevision WHERE fiPicture = NEW.idPicture) THEN
		INSERT INTO tblRevision VALUES (NULL, "orig", "Initial revision", CURRENT_TIMESTAMP(), NEW.idPicture);
	END IF;
END
END $$

DELIMITER ;
